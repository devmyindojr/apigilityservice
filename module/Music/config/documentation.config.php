<?php
return array(
    'Music\\V1\\Rpc\\Ping\\Controller' => array(
        'description' => 'Ping the API for Availability',
        'GET' => array(
            'response' => '{
   "ack": "Acknowledge the request with a timestamp"
}',
            'description' => 'Ping the API for availability and recieve a timestamp for acknowledgement',
        ),
    ),
);
