<?php
namespace Music\V1\Rest\Album;

use Zend\Db\Sql\Select;
use Zend\Db\Adapter\AdapterInterface;
use Zend\Paginator\Adapter\DbSelect;
use Album\Model\Album;
use OAuth2\Request;
use Zend\Http\Client;
// use Zend\Server\Method\Parameter;

class AlbumMapper
{
    protected $adapter;
    
    public function __construct(AdapterInterface $adapter)
    {
        $this->adapter = $adapter;
    }
    public function fetchAll()
    {
        $select = new Select('user');
        $paginatorAdapter =  new DbSelect($select, $this->adapter);
        $collection =  new AlbumCollection($paginatorAdapter);
        
        return $collection;
    }
    public function fetchOne($albumId)
    {
        $sql = 'SELECT * FROM user WHERE id = ?';
        $resultset = $this->adapter->query($sql, array($albumId));
        $data = $resultset->toArray();
        
        if(!$data)
        {
            return false;
        }
        
        $entity = new AlbumEntity();
        $entity->excangeArray($data[0]);
        return $entity;
    }
    public function deleteAction($id)
    {
        
//         $request = new Request();
//         $request->getHeaders()->addHeaders(array(
//             'Content-Type' => 'application/vnd.music.V1+json'
//         ));
//         $request->setUri('http://localhost:1990/album/19');
//         $request->setMethod('DELETE');
//         $client = new Client();
//         $response = $client->dispatch($request);
    }
}