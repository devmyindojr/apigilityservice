<?php
namespace Music\V1\Rest\Album;

use Zend\Paginator\Paginator;

class AlbumCollection extends Paginator
{
    public function __construct($paginatorAdapter)
    {
        $this->adapter = $paginatorAdapter;
    }
}
