<?php
namespace Music\V1\Rest\Album;

class AlbumEntity
{
    public $id;
    public $fullname;
    public $username;
    public $email;
    public $password;
    
    public function getArrayCopy()
    {
        return array(
            'id' => $this->id,
            'fullname' => $this->fullname,
            'username' => $this->username,
            'email'    => $this->email,
//             'password' => $this->password,
//             'artist' => $this->artist,
//             'title' => $this->title
        );
    }
    public function excangeArray(array $array)
    {
        $this->id = $array['id'];
        $this->fullname = $array['fullname'];
        $this->username = $array['username'];
        $this->email    = $array['email'];
//         $this->password = $array['password'];
//         $this->artist = $array['artist'];
//         $this->title = $array['title'];
    }
}
